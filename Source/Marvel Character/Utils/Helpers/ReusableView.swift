//
//  ReusableView.swift
//  Marvel Character
//
//  Created by William on 11/3/19.
//  Copyright © 2019 William. All rights reserved.
//

import UIKit

public protocol ReusableView: class {

    static var reuseIdentifier: String { get }
    static var nib: UINib { get }
}

extension ReusableView {

    public static var reuseIdentifier: String {

        return String(describing: self)
    }

    public static var nib: UINib {

        return UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
}
