//
//  String+Localized.swift
//  Marvel Character
//
//  Created by William on 11/18/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

extension String {

    func localized(bundle: Bundle = .main, tableName: String = "Localized") -> String {

        return NSLocalizedString(self, tableName: tableName, value: "**\(self)**", comment: "")
    }
}
