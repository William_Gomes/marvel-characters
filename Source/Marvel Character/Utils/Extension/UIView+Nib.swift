//
//  UIView+Nib.swift
//  Marvel Character
//
//  Created by William on 11/3/19.
//  Copyright © 2019 William. All rights reserved.
//

import UIKit

extension UIView {

    static func fromNib() -> Self {

        func instanceFromNib<T: UIView>() -> T {

            return UINib(nibName: "\(self)", bundle: nil).instantiate(withOwner: nil, options: nil).first as! T
        }

        return instanceFromNib()
    }
}
