//
//  UIRefreshControll+ReloadData.swift
//  Marvel Character
//
//  Created by William on 11/18/19.
//  Copyright © 2019 William. All rights reserved.
//

import UIKit

extension UIRefreshControl {

    func beginRefreshing(in collectionView: UICollectionView) {

        let offsetPoint = CGPoint(x: 0, y: -frame.size.height)
        collectionView.setContentOffset(offsetPoint, animated: true)

        beginRefreshing()
    }
}
