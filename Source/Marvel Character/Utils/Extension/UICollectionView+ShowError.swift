//
//  UICollectionView+ShowError.swift
//  Marvel Character
//
//  Created by William on 11/19/19.
//  Copyright © 2019 William. All rights reserved.
//

import UIKit

extension UICollectionView {

    func showError(viewController: UIViewController? = nil, error: Error, didTapRetryButton: @escaping () -> Void) {

        if let loadingCell = visibleCells.first(where: { collectionViewCell  in
            
            return collectionViewCell is LoadingCollectionViewCell
            
        }) as? LoadingCollectionViewCell {
            
            loadingCell.showError(error: error)
            loadingCell.didTapRetryButton = {
                
                didTapRetryButton()
            }
        } else {
            
            viewController?.showAlert(title: Localized.Error.title.rawValue.localized(),
                                      message: error.localizedDescription)
        }
    }
}
