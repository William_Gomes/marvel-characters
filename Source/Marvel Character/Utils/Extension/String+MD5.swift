//
//  String+MD5.swift
//  Marvel Character
//
//  Created by William on 11/2/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG

extension String {

    func md5() -> String! {
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = CUnsignedInt(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLength = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLength)

        CC_MD5(str!, strLen, result)

        let hash = NSMutableString()

        for i in 0..<digestLength {
            hash.appendFormat("%02x", result[i])
        }

        return String(format: hash as String)
    }
}
