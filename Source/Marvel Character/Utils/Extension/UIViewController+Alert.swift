//
//  UIViewController+Alert.swift
//  Marvel Character
//
//  Created by William on 11/16/19.
//  Copyright © 2019 William. All rights reserved.
//

import UIKit

extension UIViewController {

    func showAlert(title: String, message: String) {

        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)

        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
}
