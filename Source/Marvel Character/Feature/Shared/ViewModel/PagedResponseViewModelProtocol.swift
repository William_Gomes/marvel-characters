//
//  ListViewModelProtocol.swift
//  Marvel Character
//
//  Created by William on 11/17/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

protocol PagedResponseViewModelProtocol {

    associatedtype Model: Codable

    var delegate: PagedResponseViewModelDelegate? { get }
    var items: [Model] { get }
    var itemsCount: Int { get }
    var hasMoreContentToLoad: Bool { get }
    var totalOffset: Int { get }
    var totalPerPages: Int { get }
    var currentOffset: Int { get }
    var isLoading: Bool { get }

    func getItems()
    func getMoreItems()
    func getItem(at index: Int) -> Model
}
