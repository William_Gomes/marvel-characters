//
//  PagedResponseViewModelDelegate.swift
//  Marvel Character
//
//  Created by William on 11/17/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

protocol PagedResponseViewModelDelegate: class {

    func didSuccessGetItemsList()
    func didFailGetGetItemsList(error: Error)
    func getNetworkingRequestManager(offset: Int) -> NetworkingRequestManager
}
