//  PagedResponseViewModel.swift
//  Marvel Character
//
//  Created by William on 11/17/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

class PagedResponseViewModel<M: Codable>: PagedResponseViewModelProtocol {

    private let networkingService = NetworkingService<PagedResponseModel<M>>()

    weak var delegate: PagedResponseViewModelDelegate?

    var items: [M] = []
    var totalOffset = 0
    var totalPerPages = 20
    var currentOffset = 0
    var isLoading = false

    var hasMoreContentToLoad: Bool {
        
        return currentOffset < totalOffset && totalOffset > 0
    }

    var itemsCount: Int {

        return items.count
    }

    func getItems() {

        fetchItemsList(refresh: true)
    }

    func getMoreItems() {

        if hasMoreContentToLoad {

            fetchItemsList()
        }
    }

    func getItem(at index: Int) -> M {

        return items[index]
    }

    private func fetchItemsList(refresh: Bool = false) {

        if !isLoading {

            isLoading = true
            currentOffset = refresh ? 0 : currentOffset

            if let request = delegate?.getNetworkingRequestManager(offset: currentOffset) {

                networkingService.getItens(networkingRequestManager: request,
                                           onSuccess: { [weak self] pagedResponse in

                                            self?.currentOffset += pagedResponse.count
                                            self?.totalOffset = pagedResponse.totalResults

                                            if refresh {

                                                self?.items = pagedResponse.results

                                            } else {

                                                self?.items.append(contentsOf: pagedResponse.results)
                                            }

                                            self?.isLoading = false
                                            self?.delegate?.didSuccessGetItemsList()

                }, onError: { [weak self] error in

                    self?.isLoading = false
                    self?.delegate?.didFailGetGetItemsList(error: error)
                })

            } else {

                let error = NSError.buildError(domain: "ViewModel",
                                               code: 999,
                                               title: "Error",
                                               description: "request not found")

                delegate?.didFailGetGetItemsList(error: error)
            }
        }
    }
}
