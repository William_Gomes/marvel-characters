//
//  LoadingCollectionViewCell.swift
//  Marvel Character
//
//  Created by William on 11/3/19.
//  Copyright © 2019 William. All rights reserved.
//

import UIKit

class LoadingCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var messageButton: UIButton!

    var didTapRetryButton: (() -> Void)?

    override func awakeFromNib() {

        super.awakeFromNib()

        setInitialState()
        StyleSheet.Button.retry.apply(to: messageButton)
    }

    override func prepareForReuse() {

        super.prepareForReuse()

        setInitialState()
    }

    func showError(error: Error) {

        let retry = Localized.LoadingCell.retry.rawValue.localized()

        messageButton.isHidden = false
        messageButton.setTitle("\(error.localizedDescription)\n\(retry)", for: .normal)
        activityIndicator?.isHidden = true
    }
    
    @IBAction private func didTapRetryButton(sender: UIButton) {
        
        messageButton.isHidden = true
        activityIndicator?.isHidden = false
        didTapRetryButton?()
    }

    private func setInitialState() {

        activityIndicator?.startAnimating()
        messageButton.isHidden = true
        activityIndicator?.isHidden = false
    }
}
