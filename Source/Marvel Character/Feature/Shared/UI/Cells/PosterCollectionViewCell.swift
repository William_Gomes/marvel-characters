//
//  PosterCollectionViewCell.swift
//  Marvel Character
//
//  Created by William on 11/3/19.
//  Copyright © 2019 William. All rights reserved.
//

import Kingfisher
import UIKit

class PosterCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var thumbnailImageView: UIImageView!

    override func awakeFromNib() {

        super.awakeFromNib()

        StyleSheet.View.clearBackgroundColor.apply(to: contentView)
        StyleSheet.Label.description.apply(to: nameLabel)
        StyleSheet.ImageView.poster.apply(to: thumbnailImageView)
    }

    func configureCell(with character: CharacterModel) {

        nameLabel.text = character.name
        thumbnailImageView.kf.setImage(with: character.thumbnail.portraitImageURL,
                                       placeholder: Asset.placeholder,
                                       options: [.cacheMemoryOnly])
    }

    func configureCell(with collectionItem: CollectionItemModel) {

        nameLabel.text = collectionItem.title
        thumbnailImageView.kf.setImage(with: collectionItem.thumbnail?.portraitImageURL,
                                       placeholder: Asset.placeholder,
                                       options: [.cacheMemoryOnly])
    }
}
