//
//  EventsViewModelProtocol.swift
//  Marvel Character
//
//  Created by William on 11/17/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

protocol EventsViewModelProtocol: PagedResponseViewModel<CollectionItemModel> {

    var eventsViewModelDelegate: EventsViewModelDelegate? { get }
    var collectionModel: CollectionModel { get }

    init(eventsViewModelDelegate: EventsViewModelDelegate, collectionModel: CollectionModel)
}
