//
//  EventsViewModel.swift
//  Marvel Character
//
//  Created by William on 11/17/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

class EventsViewModel: PagedResponseViewModel<CollectionItemModel>, EventsViewModelProtocol {

    weak var eventsViewModelDelegate: EventsViewModelDelegate?

    var collectionModel: CollectionModel

    required init(eventsViewModelDelegate: EventsViewModelDelegate, collectionModel: CollectionModel) {

        self.collectionModel = collectionModel
        self.eventsViewModelDelegate = eventsViewModelDelegate

        super.init()
        delegate = self
    }
}

extension EventsViewModel: PagedResponseViewModelDelegate {

    func didSuccessGetItemsList() {

        eventsViewModelDelegate?.didSuccessGetEventList()
    }

    func didFailGetGetItemsList(error: Error) {

        eventsViewModelDelegate?.didFailGetGetEventList(with: error)
    }

    func getNetworkingRequestManager(offset: Int) -> NetworkingRequestManager {

        return NetworkingRequestManager.getCollection(url: collectionModel.collectionURL, offset: offset)
    }
}
