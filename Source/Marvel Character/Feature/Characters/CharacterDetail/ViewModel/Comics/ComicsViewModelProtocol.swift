//
//  ComicsViewModelProtocol.swift
//  Marvel Character
//
//  Created by William on 11/17/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

protocol ComicsViewModelProtocol: PagedResponseViewModel<CollectionItemModel> {

    var comicViewModelDelegate: ComicViewModelDelegate? { get }
    var collectionModel: CollectionModel { get }

    init(comicViewModelDelegate: ComicViewModelDelegate, collectionModel: CollectionModel)
}
