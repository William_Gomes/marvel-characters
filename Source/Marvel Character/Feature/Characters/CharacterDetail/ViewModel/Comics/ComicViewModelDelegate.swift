//
//  ComicViewModelDelegate.swift
//  Marvel Character
//
//  Created by William on 11/17/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

protocol ComicViewModelDelegate: class {

    func didSuccessGetComicList()
    func didFailGetGetComicList(with error: Error)
}
