//
//  ComicsViewModel.swift
//  Marvel Character
//
//  Created by William on 11/17/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

class ComicsViewModel: PagedResponseViewModel<CollectionItemModel>, ComicsViewModelProtocol {
    
    weak var comicViewModelDelegate: ComicViewModelDelegate?
    
    var collectionModel: CollectionModel
    
    required init(comicViewModelDelegate: ComicViewModelDelegate,
                  collectionModel: CollectionModel) {
        
        self.collectionModel = collectionModel
        self.comicViewModelDelegate = comicViewModelDelegate
        
        super.init()
        delegate = self
    }
}

extension ComicsViewModel: PagedResponseViewModelDelegate {

    func didSuccessGetItemsList() {

        comicViewModelDelegate?.didSuccessGetComicList()
    }

    func didFailGetGetItemsList(error: Error) {

        comicViewModelDelegate?.didFailGetGetComicList(with: error)
    }

    func getNetworkingRequestManager(offset: Int) -> NetworkingRequestManager {

        return NetworkingRequestManager.getCollection(url: collectionModel.collectionURL, offset: offset)
    }
}
