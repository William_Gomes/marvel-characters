//
//  SerieViewModelDelegate.swift
//  Marvel Character
//
//  Created by William on 11/17/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

protocol SeriesViewModelDelegate: class {

    func didSuccessGetSerieList()
    func didFailGetSerieList(with error: Error)
}
