//
//  SerieViewModel.swift
//  Marvel Character
//
//  Created by William on 11/17/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

class SeriesViewModel: PagedResponseViewModel<CollectionItemModel>, SeriesViewModelProtocol {

    weak var seriesViewModelDelegate: SeriesViewModelDelegate?

    var collectionModel: CollectionModel

    required init(seriesViewModelDelegate: SeriesViewModelDelegate,
                  collectionModel: CollectionModel) {
        
        self.collectionModel = collectionModel
        self.seriesViewModelDelegate = seriesViewModelDelegate

        super.init()
        delegate = self
    }
}

extension SeriesViewModel: PagedResponseViewModelDelegate {

    func didSuccessGetItemsList() {

        seriesViewModelDelegate?.didSuccessGetSerieList()
    }

    func didFailGetGetItemsList(error: Error) {

        seriesViewModelDelegate?.didFailGetSerieList(with: error)
    }

    func getNetworkingRequestManager(offset: Int) -> NetworkingRequestManager {

        return NetworkingRequestManager.getCollection(url: collectionModel.collectionURL, offset: offset)
    }
}
