//
//  SeriesViewModelProtocol.swift
//  Marvel Character
//
//  Created by William on 11/17/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

protocol SeriesViewModelProtocol: PagedResponseViewModel<CollectionItemModel> {

    var seriesViewModelDelegate: SeriesViewModelDelegate? { get }
    var collectionModel: CollectionModel { get }

    init(seriesViewModelDelegate: SeriesViewModelDelegate, collectionModel: CollectionModel)
}
