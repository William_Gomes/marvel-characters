//
//  CharacterDetailViewModel.swift
//  Marvel Character
//
//  Created by William on 11/16/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

class CharacterDetailViewModel: CharacterDetailViewModelProtocol {

    var characterModel: CharacterModel
    var comicsViewModel: ComicsViewModelProtocol
    var seriesViewModel: SeriesViewModelProtocol
    var eventsViewModel: EventsViewModelProtocol
   
    init(characterModel: CharacterModel,
         comicsViewModel: ComicsViewModelProtocol,
         seriesViewModel: SeriesViewModelProtocol,
         eventsViewModel: EventsViewModelProtocol) {

        self.characterModel = characterModel
        self.comicsViewModel = comicsViewModel
        self.seriesViewModel = seriesViewModel
        self.eventsViewModel = eventsViewModel
    }
}
