//
//  CharacterDetailViewModelProtocol.swift
//  Marvel Character
//
//  Created by William on 11/16/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

protocol CharacterDetailViewModelProtocol: class {

    var characterModel: CharacterModel { get }
    var comicsViewModel: ComicsViewModelProtocol { get }
    var seriesViewModel: SeriesViewModelProtocol { get }
    var eventsViewModel: EventsViewModelProtocol { get }
}
