//
//  CharacterDetailViewController.swift
//  Marvel Character
//
//  Created by William on 11/16/19.
//  Copyright © 2019 William. All rights reserved.
//

import UIKit

class CharacterDetailViewController: UIViewController {

    @IBOutlet private weak var characterImageView: UIImageView!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var seriesTitleLabel: UILabel!
    @IBOutlet private weak var seriesCollectionView: UICollectionView!
    @IBOutlet private weak var comicsTitleLabel: UILabel!
    @IBOutlet private weak var comicsCollectionView: UICollectionView!
    @IBOutlet private weak var eventsTitleLabel: UILabel!
    @IBOutlet private weak var eventsCollectionView: UICollectionView!

    private let cellSpacing: CGFloat = 20

    private var seriesDataSource: CharacterDetailDataSource!
    private var eventsDataSource: CharacterDetailDataSource!
    private var comicsDataSource: CharacterDetailDataSource!

    var viewModel: CharacterDetailViewModelProtocol!

    override func viewDidLoad() {

        super.viewDidLoad()

        title = viewModel.characterModel.name
        descriptionLabel.text = viewModel.characterModel.description
        characterImageView.kf.setImage(with: viewModel.characterModel.thumbnail.landscapeImageURL,
                                       placeholder: Asset.placeholder,
                                       options: [.cacheMemoryOnly])

        viewModel.seriesViewModel.getItems()
        viewModel.comicsViewModel.getItems()
        viewModel.eventsViewModel.getItems()

        setDataSources()
        setViewStyle()
    }

    private func setViewStyle() {

        StyleSheet.View.blackBackgroundColor.apply(to: view)
        StyleSheet.Label.description.apply(to: descriptionLabel)
        StyleSheet.View.clearBackgroundColor.apply(to: seriesCollectionView)
        StyleSheet.Label.description.apply(to: seriesTitleLabel)
        
        buildTitleLabel(localizedText: Localized.CharacterDetail.Series.rawValue.localized(),
                        total: viewModel.characterModel.serieCollectionModel.available,
                        label: seriesTitleLabel)
        
        StyleSheet.View.clearBackgroundColor.apply(to: eventsCollectionView)
        StyleSheet.Label.description.apply(to: eventsTitleLabel)
        
        buildTitleLabel(localizedText: Localized.CharacterDetail.Events.rawValue.localized(),
                        total: viewModel.characterModel.eventCollectionModel.available,
                        label: eventsTitleLabel)
        
        StyleSheet.View.clearBackgroundColor.apply(to: comicsCollectionView)
        StyleSheet.Label.description.apply(to: comicsTitleLabel)
        
        buildTitleLabel(localizedText: Localized.CharacterDetail.Comics.rawValue.localized(),
                        total: viewModel.characterModel.comicCollectionModel.available,
                        label: comicsTitleLabel)
    }

    private func buildTitleLabel(localizedText: String, total: Int, label: UILabel) {

        label.text = "\(localizedText) (\(total))"
    }
    private func setDataSources() {

        seriesDataSource = CharacterDetailDataSource(collectionView: seriesCollectionView,
                                                     loadMoreContent: loadMoreSeries)

        eventsDataSource = CharacterDetailDataSource(collectionView: eventsCollectionView,
                                                     loadMoreContent: loadMoreEvents)

        comicsDataSource = CharacterDetailDataSource(collectionView: comicsCollectionView,
                                                     loadMoreContent: loadMoreComics)
    }

    private func showErrorOnLoading(on collectionView: UICollectionView,
                                    error: Error,
                                    didTapRetryButton: @escaping () -> Void) {

        if let loadingCell = collectionView.visibleCells.first(where: { collectionViewCell  in

            return collectionViewCell is LoadingCollectionViewCell
        }) as? LoadingCollectionViewCell {

            loadingCell.showError(error: error)
            loadingCell.didTapRetryButton = {

                didTapRetryButton()
            }
        }
    }

    private func loadMoreSeries() {

        viewModel.seriesViewModel.getMoreItems()
    }

    private func loadMoreEvents() {

        viewModel.eventsViewModel.getMoreItems()
    }

    private func loadMoreComics() {

        viewModel.comicsViewModel.getMoreItems()
    }
}

extension CharacterDetailViewController: SeriesViewModelDelegate {

    func didSuccessGetSerieList() {

        seriesDataSource.hasMoreContentToLoad = viewModel.seriesViewModel.hasMoreContentToLoad
        seriesDataSource.collectionItems = viewModel.seriesViewModel.items

        let hasContent = viewModel.seriesViewModel.itemsCount > 0
        seriesTitleLabel.isHidden = !hasContent
        seriesCollectionView.isHidden = !hasContent
        seriesCollectionView.reloadData()
    }

    func didFailGetSerieList(with error: Error) {

        seriesCollectionView.showError(error: error) { [weak self] in

            self?.viewModel.seriesViewModel.getItems()
        }
    }
}

extension CharacterDetailViewController: EventsViewModelDelegate {

    func didSuccessGetEventList() {

        eventsDataSource.hasMoreContentToLoad = viewModel.eventsViewModel.hasMoreContentToLoad
        eventsDataSource.collectionItems = viewModel.eventsViewModel.items

        let hasContent = viewModel.eventsViewModel.itemsCount > 0
        eventsTitleLabel.isHidden = !hasContent
        eventsCollectionView.isHidden = !hasContent

        eventsCollectionView.reloadData()
    }

    func didFailGetGetEventList(with error: Error) {

        eventsCollectionView.showError(error: error) { [weak self] in

            self?.viewModel.eventsViewModel.getItems()
        }
    }
}

extension CharacterDetailViewController: ComicViewModelDelegate {

    func didSuccessGetComicList() {

        comicsDataSource.hasMoreContentToLoad = viewModel.comicsViewModel.hasMoreContentToLoad
        comicsDataSource.collectionItems = viewModel.comicsViewModel.items

        let hasContent = viewModel.comicsViewModel.itemsCount > 0
        comicsTitleLabel.isHidden = !hasContent
        comicsCollectionView.isHidden = !hasContent

        comicsCollectionView.reloadData()
    }

    func didFailGetGetComicList(with error: Error) {

        comicsCollectionView.showError(error: error) { [weak self] in

            self?.viewModel.comicsViewModel.getItems()
        }
    }
}
