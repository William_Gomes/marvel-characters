//
//  SeriesCollectionViewDataSource.swift
//  Marvel Character
//
//  Created by William on 11/16/19.
//  Copyright © 2019 William. All rights reserved.
//

import UIKit

class CharacterDetailDataSource: NSObject {

    private let cellSpacing: CGFloat = 20

    var collectionItems: [CollectionItemModel] = []
    var hasMoreContentToLoad = true
    var loadMoreContent: (() -> Void)

    init(collectionView: UICollectionView, loadMoreContent: @escaping (() -> Void)) {

        self.loadMoreContent = loadMoreContent

        super.init()
        
        collectionView.registerNib(for: PosterCollectionViewCell.self)
        collectionView.registerNib(for: LoadingCollectionViewCell.self)
        
        let collectionViewLeftAlignFlowLayout = CollectionViewLeftAlignFlowLayout()
        collectionViewLeftAlignFlowLayout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = collectionViewLeftAlignFlowLayout
        
        collectionView.dataSource = self
        collectionView.delegate = self
    }

    private func isShowingLoadingCell(_ indexPath: IndexPath) -> Bool {

        return indexPath.row == collectionItems.count
    }
}

extension CharacterDetailDataSource: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return hasMoreContentToLoad ? collectionItems.count + 1 : collectionItems.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if isShowingLoadingCell(indexPath) && hasMoreContentToLoad {

            return collectionView.dequeueReusableCell(for: indexPath) as LoadingCollectionViewCell

        } else {

            let collectionItem = collectionItems[indexPath.row]
            let cell = collectionView.dequeueReusableCell(for: indexPath) as PosterCollectionViewCell
            cell.configureCell(with: collectionItem)

            return cell
        }
    }
}

extension CharacterDetailDataSource: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {

        if isShowingLoadingCell(indexPath) {

            loadMoreContent()
        }
    }
}

extension CharacterDetailDataSource: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width: CGFloat!
        let height: CGFloat!

        if isShowingLoadingCell(indexPath) && hasMoreContentToLoad {

            width = ceil(collectionView.frame.width - cellSpacing * 2)
            height = collectionView.frame.height

        } else {

            width = collectionView.frame.width / 2.5 - cellSpacing * 1.5
            height = collectionView.frame.height
        }

        return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {

        return UIEdgeInsets(top: 0, left: cellSpacing, bottom: 0, right: cellSpacing)
    }
}
