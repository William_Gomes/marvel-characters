//
//  CharactersListViewController.swift
//  Marvel Character
//
//  Created by William on 11/3/19.
//  Copyright © 2019 William. All rights reserved.
//

import UIKit

class CharactersListViewController: UIViewController {
    
    @IBOutlet private weak var collectionView: UICollectionView!
    
    private let cellSpacing: CGFloat = 20
    private let characterDetailSegue = "characterDetailSegue"
    
    private var viewModel: CharacterListViewModelProtocol!
    private var refresher: UIRefreshControl!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        title = Localized.CharacterList.title.rawValue.localized()
        StyleSheet.View.blackBackgroundColor.apply(to: view)
        StyleSheet.View.clearBackgroundColor.apply(to: collectionView)
        
        viewModel = CharacterListViewModel(characterListViewModelDelegate: self)
        setupCollectionView()
        loadData()
    }
    
    private func setupCollectionView() {
        
        collectionView.registerNib(for: PosterCollectionViewCell.self)
        collectionView.registerNib(for: LoadingCollectionViewCell.self)
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.alwaysBounceVertical = true
        collectionView.collectionViewLayout = CollectionViewLeftAlignFlowLayout()
        
        refresher = UIRefreshControl()
        collectionView.refreshControl = refresher
        StyleSheet.RefreshControll.loading.apply(to: refresher)
        refresher.addTarget(self, action: #selector(refreshData), for: .valueChanged)
    }
    
    @objc
    private func refreshData() {
        
        loadData()
    }
    
    private func loadData() {

        collectionView.refreshControl?.beginRefreshing(in: collectionView)
        
        viewModel.getItems()
    }
    
    private func isShowingLoadingCell(_ indexPath: IndexPath) -> Bool {
        
        return indexPath.row == viewModel.itemsCount
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let characterDetailViewController = segue.destination as? CharacterDetailViewController,
            let characterModel = sender as? CharacterModel {
            
            let comicsViewModel = ComicsViewModel(comicViewModelDelegate: characterDetailViewController,
                                                  collectionModel: characterModel.comicCollectionModel)
            
            let seriesViewModel = SeriesViewModel(seriesViewModelDelegate: characterDetailViewController,
                                                  collectionModel: characterModel.serieCollectionModel)
            
            let eventsViewModel = EventsViewModel(eventsViewModelDelegate: characterDetailViewController,
                                                  collectionModel: characterModel.eventCollectionModel)
            
            let viewModel = CharacterDetailViewModel(characterModel: characterModel,
                                                     comicsViewModel: comicsViewModel,
                                                     seriesViewModel: seriesViewModel,
                                                     eventsViewModel: eventsViewModel)
            
            characterDetailViewController.viewModel = viewModel
        }
    }
}

extension CharactersListViewController: CharacterListViewModelDelegate {
    
    func didSuccessGetCharactersList() {
        
        collectionView.refreshControl?.endRefreshing()
        collectionView.reloadData()
    }
    
    func didFailGetCharactersList(with error: Error) {
        
        collectionView.refreshControl?.endRefreshing()

        collectionView.showError(viewController: self, error: error) { [weak self] in

            self?.viewModel.getMoreItems()
        }
    }
}

extension CharactersListViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return viewModel.hasMoreContentToLoad ?
            viewModel.itemsCount + 1 : viewModel.itemsCount
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if isShowingLoadingCell(indexPath) && viewModel.hasMoreContentToLoad {
            
            return collectionView.dequeueReusableCell(for: indexPath) as LoadingCollectionViewCell
            
        } else {
            
            let character = viewModel.getItem(at: indexPath.row)
            let cell = collectionView.dequeueReusableCell(for: indexPath) as PosterCollectionViewCell
            cell.configureCell(with: character)
            
            return cell
        }
    }
}

extension CharactersListViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        
        if isShowingLoadingCell(indexPath) {
            
            viewModel.getMoreItems()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let characterModel = viewModel.getItem(at: indexPath.row)
        performSegue(withIdentifier: characterDetailSegue, sender: characterModel)
    }
}

extension CharactersListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width: CGFloat!
        let height: CGFloat!
        
        if isShowingLoadingCell(indexPath) {
            
            width = ceil(collectionView.frame.width - cellSpacing * 2)
            height = 50
            
        } else {
            
            width = (collectionView.frame.width / 2) - (cellSpacing + cellSpacing / 2)
            height = width * 2
        }
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: cellSpacing, left: cellSpacing, bottom: cellSpacing, right: cellSpacing)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return cellSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return cellSpacing
    }
}
