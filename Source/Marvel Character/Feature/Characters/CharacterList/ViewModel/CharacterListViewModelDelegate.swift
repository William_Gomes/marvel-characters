//
//  CharacterListViewModelDelegate.swift
//  Marvel Character
//
//  Created by William on 11/3/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

protocol CharacterListViewModelDelegate: class {

    func didSuccessGetCharactersList()
    func didFailGetCharactersList(with error: Error)
}
