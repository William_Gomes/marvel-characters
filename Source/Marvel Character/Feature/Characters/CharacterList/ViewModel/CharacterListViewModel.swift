//
//  CharacterListViewModel.swift
//  Marvel Character
//
//  Created by William on 11/3/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

class CharacterListViewModel: PagedResponseViewModel<CharacterModel>, CharacterListViewModelProtocol {

    weak var characterListViewModelDelegate: CharacterListViewModelDelegate?

    required init(characterListViewModelDelegate: CharacterListViewModelDelegate) {

        self.characterListViewModelDelegate = characterListViewModelDelegate

        super.init()
        delegate = self
    }
}

extension CharacterListViewModel: PagedResponseViewModelDelegate {

    func didSuccessGetItemsList() {

        characterListViewModelDelegate?.didSuccessGetCharactersList()
    }

    func didFailGetGetItemsList(error: Error) {

        characterListViewModelDelegate?.didFailGetCharactersList(with: error)
    }

    func getNetworkingRequestManager(offset: Int) -> NetworkingRequestManager {

        return NetworkingRequestManager.getCharacters(offset: offset)
    }
}
