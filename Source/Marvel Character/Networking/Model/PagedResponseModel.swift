//
//  PagedResponseModel.swift
//  Marvel Character
//
//  Created by William on 11/3/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

struct PagedResponseModel<T: Codable>: Codable {

    let currentOffset: Int
    let count: Int
    let totalResults: Int
    let results: [T]

    enum CodingKeys: String, CodingKey {

        case currentOffset = "offset"
        case count = "count"
        case totalResults = "total"
        case results = "results"
    }
}
