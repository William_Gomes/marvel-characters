//
//  ThumbnailModel.swift
//  Marvel Character
//
//  Created by William on 11/3/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

struct ThumbnailModel: Codable {

    private let path: String
    private let imageExtension: String

    var portraitImageURL: URL? {

        return URL(string: "\(path)/portrait_fantastic.\(imageExtension)")
    }

    var landscapeImageURL: URL? {

        return URL(string: "\(path)/standard_fantastic.\(imageExtension)")
    }

    enum CodingKeys: String, CodingKey {

        case path = "path"
        case imageExtension = "extension"
    }
}
