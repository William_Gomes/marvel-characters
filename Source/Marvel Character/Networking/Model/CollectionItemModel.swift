//
//  CollectionItemModel.swift
//  Marvel Character
//
//  Created by William on 11/16/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

struct CollectionItemModel: Codable {

    let title: String
    let thumbnail: ThumbnailModel?

    enum CodingKeys: String, CodingKey {

        case title = "title"
        case thumbnail = "thumbnail"
    }
}
