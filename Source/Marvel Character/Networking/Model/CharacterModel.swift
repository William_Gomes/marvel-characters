//
//  CharacterModel.swift
//  Marvel Character
//
//  Created by William on 11/2/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

struct CharacterModel: Codable {

    let identifier: Int
    let name: String
    let description: String?
    let thumbnail: ThumbnailModel
    let comicCollectionModel: CollectionModel
    let serieCollectionModel: CollectionModel
    let eventCollectionModel: CollectionModel

    enum CodingKeys: String, CodingKey {

        case identifier = "id"
        case name = "name"
        case description = "description"
        case thumbnail = "thumbnail"
        case comicCollectionModel = "comics"
        case serieCollectionModel = "series"
        case eventCollectionModel = "events"
    }
}
