//
//  ComicModel.swift
//  Marvel Character
//
//  Created by William on 11/16/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

struct CollectionModel: Codable {

    let available: Int
    let collectionURL: String

    enum CodingKeys: String, CodingKey {

        case available = "available"
        case collectionURL = "collectionURI"
    }
}
