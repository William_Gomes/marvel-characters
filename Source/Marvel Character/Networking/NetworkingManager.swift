//
//  NetworkingManager.swift
//  Marvel Character
//
//  Created by William on 11/2/19.
//  Copyright © 2019 William. All rights reserved.
//

import Alamofire
import Foundation

struct NetworkingManager {

    func request<P: ParserProtocol>(networkingRequestManager: NetworkingRequestManagerProtocol,
                                    parser: P,
                                    onSuccess: @escaping (P.ModelType) -> Void,
                                    onError: @escaping (Error) -> Void) {

        Alamofire.request(networkingRequestManager).validate().responseData { responseData in

            switch responseData.result {
            case .success(let data):

                let result = parser.parseData(data: data)

                switch result {

                case .success(let parsedModel):

                    onSuccess(parsedModel)

                case .failure(let error):

                    onError(error)
                }

            case .failure(let error):

                onError(error)
            }
        }
    }
}
