//
//  ParserProtocol.swift
//  Marvel Character
//
//  Created by William on 11/2/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

protocol ParserProtocol {

    associatedtype ModelType: Codable

    func parseData(data: Data) -> Result<ModelType, Error>
}
