//
//  CharacterModelParser.swift
//  Marvel Character
//
//  Created by William on 11/2/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

class BaseParser<M: Codable>: ParserProtocol {

    private let rootPath: String?

    init(rootPath: String? = "data") {

        self.rootPath = rootPath
    }

    func parseData(data: Data) -> Result<M, Error> {

        return ParserHelper.parseJSONData(rootNodeName: rootPath, data: data)
    }
}
