//
//  NetworkRequestManager.swift
//  Marvel Character
//
//  Created by William on 11/2/19.
//  Copyright © 2019 William. All rights reserved.
//

import Alamofire
import Foundation

protocol NetworkingRequestManagerProtocol: URLRequestConvertible {

    var baseURL: String { get }
    var publicAPIKey: String { get }
    var privateAPIKey: String { get }
    var timestamp: String { get }
    var relativePath: String? { get }
    var params: URLQueryItem? { get }
}

enum NetworkingRequestManager: NetworkingRequestManagerProtocol {

    case getCharacters(offset: Int)
    case getCollection(url: String, offset: Int)

    var baseURL: String {
        switch self {
        case .getCharacters:
            return "https://gateway.marvel.com:443/v1/public/"
        case .getCollection(let url, _):
            return url
        }
    }

    var publicAPIKey: String {
        return "8b37506846b543ab6c66ed28764a63ad"
    }

    var privateAPIKey: String {
        return "104b6fb5e24d8aad626970840c5ed887fa966655"
    }

    var timestamp: String {
        return "1"
    }

    var relativePath: String? {

        switch self {
        case .getCharacters:
            return "characters"
        case .getCollection:
            return nil
        }
    }

    var params: URLQueryItem? {

        switch self {
        case .getCharacters(let offset):
            return URLQueryItem(name: "offset", value: "\(offset)")
        case .getCollection(_, let offset):
            return URLQueryItem(name: "offset", value: "\(offset)")
        }
    }

    func asURLRequest() throws -> URLRequest {

        let hash = "\(timestamp)\(privateAPIKey)\(publicAPIKey)".md5()

        var queryItems = [URLQueryItem(name: "apikey", value: publicAPIKey),
                          URLQueryItem(name: "ts", value: "1"),
                          URLQueryItem(name: "hash", value: hash)]

        if let params = params {

            queryItems.append(params)
        }

        var urlComponents = URLComponents(string: "\(baseURL)\(relativePath ?? "")")!
        urlComponents.queryItems = queryItems

        var urlRequest = URLRequest(url: urlComponents.url!)
        urlRequest.timeoutInterval = 5
        urlRequest.cachePolicy = .returnCacheDataElseLoad
        let encoding = JSONEncoding(options: .prettyPrinted)

        let encodedRequest = try encoding.encode(urlRequest)
        return encodedRequest
    }
}
