//
//  NetworkingService.swift
//  Marvel Character
//
//  Created by William on 11/17/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

class NetworkingService<M: Codable> {
    
    func getItens(networkingRequestManager: NetworkingRequestManager,
                  onSuccess: @escaping (M) -> Void,
                  onError: @escaping (Error) -> Void) {

        NetworkingManager().request(networkingRequestManager: networkingRequestManager,
                                    parser: BaseParser<M>(),
                                    onSuccess: onSuccess,
                                    onError: onError)
    }
}
