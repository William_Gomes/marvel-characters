//
//  StyleSheet.swift
//  Marvel Character
//
//  Created by William on 11/18/19.
//  Copyright © 2019 William. All rights reserved.
//

import UIKit

// swiftlint:disable type_body_length

enum FontSize: CGFloat {

    case size18 = 18.0
    case size22 = 22.0
}

enum StyleSheet {

    enum View {

        static let blackBackgroundColor = Style<UIView> {

            $0.backgroundColor = UIColor.black
        }

        static let clearBackgroundColor = Style<UIView> {

            $0.backgroundColor = UIColor.clear
        }
    }

    enum Label {

        static let description = Style<UILabel> {

            $0.font = UIFont.boldSystemFont(ofSize: FontSize.size18.rawValue)
            $0.backgroundColor = UIColor.clear
            $0.textColor = UIColor.white
        }
    }

    enum CollectionView {

        static let backgroundColor = Style<UICollectionView> {

            $0.backgroundColor = UIColor.clear
        }
    }

    enum ImageView {

        static let poster = Style<UIImageView> {

            $0.layer.cornerRadius = 5
            $0.clipsToBounds = true
        }
    }

    enum Button {

        static let retry = Style<UIButton> {

            $0.titleLabel?.font = UIFont.boldSystemFont(ofSize: FontSize.size22.rawValue)
            $0.setTitleColor(UIColor.white, for: .normal)
            $0.titleLabel?.lineBreakMode = .byWordWrapping
            $0.titleLabel?.numberOfLines = 2
            $0.titleLabel?.textAlignment = .center
        }
    }

    enum RefreshControll {

        static let loading = Style<UIRefreshControl> {

            $0.tintColor = UIColor.white
        }
    }
}

// swiftlint:enable type_body_length
