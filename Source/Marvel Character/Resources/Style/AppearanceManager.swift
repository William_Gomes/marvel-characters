//
//  AppearanceManager.swift
//  Marvel Character
//
//  Created by William on 11/18/19.
//  Copyright © 2019 William. All rights reserved.
//

import UIKit

class AppearanceManager {

    private init() { }

    static func setAppearance() {

        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = UIColor.white

        let attributes: [NSAttributedString.Key: Any]
        attributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: FontSize.size22.rawValue),
                      NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationBarAppearace.titleTextAttributes = attributes
        navigationBarAppearace.isTranslucent = false
        navigationBarAppearace.barTintColor = UIColor.black

    }
}
