//
//  Style.swift
//  Marvel Character
//
//  Created by William on 11/18/19.
//  Copyright © 2019 William. All rights reserved.
//

import UIKit

public struct Style<View: UIView> {

    public let style: (View) -> Void

    public init(_ style: @escaping (View) -> Void) {
        self.style = style
    }

    public func apply(to view: View) {
        style(view)
    }
}
