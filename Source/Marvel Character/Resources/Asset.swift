//
//  ImageAssetManager.swift
//  Marvel Character
//
//  Created by William on 11/3/19.
//  Copyright © 2019 William. All rights reserved.
//

import UIKit

enum Asset {

    static let placeholder = UIImage(named: "placeholder")
    static let launch = UIImage(named: "launch")
}
