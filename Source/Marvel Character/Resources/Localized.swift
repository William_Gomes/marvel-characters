//
//  Localized.swift
//  Marvel Character
//
//  Created by William on 11/18/19.
//  Copyright © 2019 William. All rights reserved.
//

import Foundation

enum Localized {

    enum LoadingCell: String {

        case retry = "LoadingCell.Button.retry"
    }

    enum Error: String {

           case title = "Error.title"
       }

    enum CharacterList: String {

        case title = "CharacterList.Title"
    }

    enum CharacterDetail: String {

        case Comics = "CharacterDetail.Label.Comics"
        case Events = "CharacterDetail.Label.Events"
        case Series = "CharacterDetail.Label.Series"
    }
}
