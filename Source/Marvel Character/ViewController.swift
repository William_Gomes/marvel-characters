//
//  ViewController.swift
//  Marvel Character
//
//  Created by William on 11/2/19.
//  Copyright © 2019 William. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {

        super.viewDidLoad()

        let networkRequestManager = NetworkingRequestManager.getCharacters
        NetworkingManager().request(networkingRequestManager: networkRequestManager,
                                    parser: CharacterModelParser(),
                                    onSuccess: { parsedModel in
                                        
                                        debugPrint(parsedModel)
        }, onError: { error in

            debugPrint(error)
        })

    }
}
