//
//  SceneDelegate.swift
//  Marvel Character
//
//  Created by William on 11/2/19.
//  Copyright © 2019 William. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene,
               willConnectTo session: UISceneSession,
               options connectionOptions: UIScene.ConnectionOptions) {

        if (scene as? UIWindowScene) == nil {

            return
        }
    }
}
